#ifndef BBU_H
#define BBU_H
#include "metadata.h"

class Bbu{
    public :
        int charge;
        
        Bbu(); //constructeur
        void addCharge();
        void removeCharge();
};
#endif