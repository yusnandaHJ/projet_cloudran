#ifndef RRH_H
#define RRH_H
#include "metadata.h"

class Rrh{
    public :
        int charge;//ni
        int strategy[BBU_MAX];//Yi
        int cost;//ci
        
        Rrh();//constructeur
        void setStrategy(int bbu1, int bbu2);
        void calculateCost(int G[SURFACE][SURFACE]);//calcul de ci
};
#endif