#include <iostream>
#include "Bbu.h"
#include "Rrh.h"
#include <time.h>
#include <stdlib.h>
#include "metadata.h"

void oneToOneSimulation(Bbu bbus[], Rrh rrhs[], int G[SURFACE][SURFACE]);
void normalSimulation();