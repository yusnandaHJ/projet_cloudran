#include "cloudRAN_simulator.h"

using namespace std;

int main(int argc, char* argv[]){
    srand(time(NULL));
    Bbu bbus[BBU_MAX];
    Rrh rrhs[RRH_MAX];
    int G[SURFACE][SURFACE]={0}; //matrice G des interférences
    
    /*initialisation de G*/
    for(int i = 0; i < SURFACE; i++){
        for(int j=0 ; j < SURFACE ; j++){
            if(i != j){
                G[i][j] = rand()%4+1;
            }
        }
    }
    
    /*SIMULATION 1 BBU : 6 RRH (1:1)*/
    oneToOneSimulation(bbus,rrhs,G); //à boucler pour avoir plusieurs valeurs ?
    
    /*SIMULATION 2 BBU et 6 RRH*/
    normalSimulation();//à boucler pour avoir plusieurs valeurs ? 
    return 0;
}

void oneToOneSimulation(Bbu bbus[], Rrh rrhs[], int G[SURFACE][SURFACE]){
    /******TODO : test de la fonction ******/
    float meanCost = 0;
    /*Nous mettons tous les RRHs sur le BBU1*/
    for(int i = 0; i < RRH_MAX; i++){
        rrhs[i].setStrategy(1,0);
    }
    
    /*Nous calculons tous les Ci*/
    for(int i=0; i < RRH_MAX; i++){
        rrhs[i].calculateCost(G);
    }
    
    /*Nous calculons le cout moyen*/
    for(int i = 0; i < RRH_MAX; i++){
        meanCost += rrhs[i].cost;
    }
    meanCost = meanCost/RRH_MAX;
    
    /*Affichage des resultats*/
    cout << meanCost << endl;
}

void normalSimulation(){
    /*******TODO******/
    return;
}