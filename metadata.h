#ifndef METADATA_H
#define metadata
//dimension de la matrice G
    #define SURFACE 7

//nombre de BBU
    #define BBU_MAX 2

//nombre de RRH
    #define RRH_MAX 6
    
//nombre de UE max par RRH
    #define UE_MAX 5
    
//facteur de préférence alpha pour le cout d'un rrh
    #define ALPHA 0.5

//N0
    #define N0 0.01
#endif